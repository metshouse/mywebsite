			<div class="row">
				<div class="col-12">
						<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					      		<div class="modal-header">
					        		<h5 class="modal-title text-center" id="exampleModalLabel">Sign In</h5>

					        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          			<span aria-hidden="true">&times;</span>
					        		</button>
					      		</div>
					      		
					      	<!-- Login with Google and Fb -->
					      	
						        <div class ="text-center">
									<p class ="m-0 my-2">With</p>
								</div>				
								<div class="row p-0 m-0">
									<div class="col-6 text-center pr-0">
										<div class="signin-connect-facebook mr-2 ">
											<span>Facebook</span>
										</div>
									</div>
									<div class="col-6 text-center pl-0">
										<div class="signin-connect-google ml-2">
											<span>Google</span>
										</div>
									</div>
								</div>											
								<div class ="text-center">
									<p class ="m-0 mt-2">Or</p>
								</div> 
								
								<!-- END of Login with Google and Fb -->

					      		<div class="modal-body">
							        <?php
							        echo form_open('Account/sign_in_validation');
							        ?>
								        <div class="form-group">
										    <label>E-mail</label>
										    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="E-mail Address">
										    <div class="alert alert-primary" id="email-error" role="alert" hidden>
												  This is a primary alert—check it out!
												</div>
										    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
										    </small> -->
										</div>
										<div class="form-group">
										  	<label>Password</label>
										    <input type="password" class="form-control" id="" placeholder="Password">
										</div>
										<div class="form-group">
										    <label></label>
										     <a href="#" class="">Forgot Account?</a>									    
										</div>							       	
					      		</div>

						      		<div class="modal-footer">					      	
										<div class="col-7">
											<a class="btn btn-dark" href="<?php echo base_url('account/register_account_page')?>">Sign Up</a>	
										</div>
										<div class="col-3 p-0 text-right">			
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
									    </div>	
									    <div class="col-2 p-0 text-right">
									        <button type="submit" class="btn btn-dark">Log In</button>
									    </div>	
								       
								     </div>

							       <?php
							        echo form_close();
							        ?>
					    	</div>
					  	</div>
					</div>
				</div>
			</div>