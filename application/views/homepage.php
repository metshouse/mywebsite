	</div>
	<div class="container jumboltron">
	
	</div>
	
	<div class="container thumbnail_button">
		<!-- home thumbnail -->
		 <div class="row">
		 	<div class="col p-0">
		 		<button type="button" class="portrait">PORTRAIT</button>
		 	</div>
		 	<div class="col p-0">
		 		<button type="button" class="webdesign">WEB DESIGN</button>
		 	</div>
		 	<div class="col p-0">
		 		<button type="button" class="arts">ARTWORK</button>
		 	</div>
		 </div>

		 <div class="row" id="mainGallery">
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work3.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work3.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work3.jpg')?>" class="img-thumbnail p-0">
		 	</div>	

		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work3.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work3.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work3.jpg')?>" class="img-thumbnail p-0">
		 	</div>	
		 </div>


		 <div class="row" id="portrait">
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>	

		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>	
		 </div>
		 <div class="row" id="webDesign">
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>	

		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>
		 	<div class="col-md-4 col-sm-6 p-0">
		 		<img src="<?php echo base_url('theme/image/work4.jpg')?>" class="img-thumbnail p-0">
		 	</div>	
		 </div>

		 <!-- end of home thumbnail -->

		 <!-- contact -->
		 <dov class="row">
		 	<div class="col-md-4 col-sm-4 col-s-4 info ">
		 		<h6 class="mt-3">Contact:</h6>
		 		<p class="mt-4">jayrex_16@hotmail.com</p>
		 		<p>+639995957928</p>
		 		<h6>Follow:</h6>
		 		<img src="<?php echo base_url('theme/image/followimage.png')?>">
		 	</div>
		 	
		 	<div class="col-md-4 col-sm-4 col-sm-4 info contact_divider">
		 		<h6 class="mt-3">Message me:</h6>

		 		<form>
		 			<div class="input-group">
		 			<br>
		 			<input class="form-control mt-3" type="text" name="email" placeholder="Your Email">
		 			</div>
		 			<div class="input-group">
		 			<br>
		 			<input class="form-control mt-3" type="text" name="subject" placeholder="Subject">
		 			</div>
		 			<div class="input-group">
		 			<br>
		 			<textarea class="form-control mt-3" id="message_box" type="text" name="message" placeholder="Your Message" cols="3" maxlength="200"></textarea>
		 			</div>
		 			<div class="input-group">
		 			<br>
		 			<button type="submit" class="btn btn btn-success mt-3 mx-auto">Send Message</button>
		 			</div>
		 		</form>
		 	</div>
		 	<!-- end contact -->

		 	
		 	<div class="col-md-4 col-sm-4 col-sm-4 info contact_divider">
		 		
		 	</div>
		 </dov>



