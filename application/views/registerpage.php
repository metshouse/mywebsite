		<div class="register_page mainpage_padding_top">	
			<div class="row" >
				<div class="col-12">
					<div class="row">
						<div class="col-md-3 col-sm-2">
							
						</div>
						<div class="col-md-6 col-sm-8 register_form_style mb-3 px-5">
							<div class ="mt-3 text-center">
								<h5 class="mb-3">Sign Up</h5>

								<div class="dropdown-divider mb-3"></div> <!-- divider -->

								<p>With</p>
							</div>
							
							<div class="row text-center mx-auto mb-3">
								<div class="connect-facebook col social-box mr-1">
									<span>Facebook</span>
								</div>
								
								<div class="connect-google col social-box ml-1">
									<span>Google</span>
								</div>
							</div>
							<div class =" text-center">
								<p>Or</p>
							</div>

							<div class="dropdown-divider mb-3"></div> <!-- divider -->
							<div class="row">
								<div class="col-12">
									<?php echo validation_errors(); ?>
								</div>
							</div>
							<?php
							echo form_open('Account/sign_up_validation');
																
							?>	
								<div class="row">
									<div class="col-6">
									<div class="form-group">
										<label>Full Name *</label>
											<input class="form-control" type="text" name="full_name" value="">	
									</div>
									</div>
									<div class="col-6">
									<div class="form-group">
										<label>Birthday *</label>
											<input class="form-control" type="date" name="birthday" value="">	
									</div>
									</div>
								</div>

								<div class="form-group">
									<label>Email *</label>
										<input class="form-control" type="email" name="email" value="">	
								</div>
								<div class="form-group">
									<label>Password *</label>
										<input class="form-control" type="password" name="password" value="">		
								</div>
								<div class="form-group">
									<label>Confirm Password *</label>
										<input class="form-control" type="password" name="confirm_password" value="">		
								</div>
																							
								<div class=" my-2">Gender *</div>
								<div class="form-check">
									<label class="form-check-label my-1">
										<input class="form-check-input" type="radio" name="gender" id="" value="M"><span class="pr-2">Male</span>
									</label>
									
									<label class="form-check-label my-1">
										<input class="form-check-input" type="radio" name="gender" id="" value="F"><span class="pr-2">Female</span>
									</label>	
								</div>

								<div class="dropdown-divider mb-3"></div> <!-- divider -->

								<div class="text-center">
									<button type="submit" class="btn btn-dark mb-4 mr-2">Sign Up</button>
									<a class="btn btn-secondary mb-4" href="<?php echo base_url('home')?>">Cancel</a>
									
								</div>
							<?php
							echo form_close();
							?>
						</div>
						<div class="col-md-3 col-sm-2 ">
							
						</div>
					</div>
				</div>
			</div>
		</div>