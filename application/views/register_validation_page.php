		<div class="register_page mainpage_padding_top">	
			<div class="row" >
				<div class="col-12">
					<div class="row">
						<div class="col-md-3 col-sm-2">
							
						</div>
						<div class="col-md-6 col-sm-8 register_form_style mb-3 px-5">
							<div class ="mt-3 text-center">
								<h5 class="mb-3">Sign Up</h5>

								<div class="dropdown-divider mb-3"></div> <!-- divider -->

								<p>With</p>
							</div>
							
							<div class="row text-center mx-auto mb-3">
								<div class="connect-facebook col social-box mr-1">
									<span>Facebook</span>
								</div>
								
								<div class="connect-google col social-box ml-1">
									<span>Google</span>
								</div>
							</div>
							<div class =" text-center">
								<p>Or</p>
							</div>

							<div class="dropdown-divider mb-3"></div> <!-- divider -->
							<div class="row">
								<div class="col-12">
									<?php echo validation_errors(); ?>
								</div>
							</div>
							<?php
							echo form_open('Account/sign_up_validation');
																
							?>	
								<div class="row">
									<div class="col-6">
									<div class="form-group">
										<label>Full Name *</label>
											<input class="form-control" type="text" name="full_name" value="<?php echo $full_name; ?>">	
									</div>
									</div>
									<div class="col-6">
									<div class="form-group">
										<label>Birthday *</label>
											<input class="form-control" type="date" name="birthday" value="<?php echo $birthday; ?>">	
									</div>
									</div>
								</div>
								<!-- <div class="form-group">
									<label>Birthday</label>									
									<div class="row">							
										<div class="col-4">
											<select class="form-control" name="">
												<option value="" selected="selected">Day</option>
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
												<option value="06">06</option>
												<option value="07">07</option>
												<option value="08">08</option>
												<option value="09">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
												<option value="13">13</option>
												<option value="14">14</option>
												<option value="15">15</option>
												<option value="16">16</option>
												<option value="17">17</option>
												<option value="18">18</option>
												<option value="19">19</option>
												<option value="20">20</option>
												<option value="21">21</option>
												<option value="22">22</option>
												<option value="23">23</option>
												<option value="24">24</option>
												<option value="25">25</option>
												<option value="26">26</option>
												<option value="27">27</option>
												<option value="28">28</option>
												<option value="29">29</option>
												<option value="30">30</option>
												<option value="31">31</option>		
											</select>
										</div>
										<div class="col-4">
											<select class="form-control" name="">
												<option value="" selected="selected">Month</option>
												<option value="January">January</option>
												<option value="February">February</option>
												<option value="March">March</option>
												<option value="April">April</option>
												<option value="May">May</option>
												<option value="June">June</option>
												<option value="July">July</option>
												<option value="August">August</option>
												<option value="September">September</option>
												<option value="October">October</option>
												<option value="November">November</option>
												<option value="December">December</option>		
											</select>
										</div>
										<div class="col-4">
											<select class="form-control" name="">
												<option value="" selected="selected">Year</option>
												<option value="2017">2017</option>
												<option value="2016">2016</option>
												<option value="2015">2015</option>
												<option value="2014">2014</option>
												<option value="2013">2013</option>
												<option value="2012">2012</option>
												<option value="2011">2011</option>
												<option value="2010">2010</option>
												<option value="2009">2009</option>
												<option value="2008">2008</option>
												<option value="2007">2007</option>
												<option value="2006">2006</option>
												<option value="2005">2005</option>
												<option value="2004">2004</option>
												<option value="2003">2003</option>
												<option value="2002">2002</option>
												<option value="2001">2001</option>
												<option value="2000">2000</option>
												<option value="1999">1999</option>
												<option value="1998">1998</option>
												<option value="1997">1997</option>
												<option value="1996">1996</option>
												<option value="1995">1995</option>
												<option value="1994">1994</option>
												<option value="1993">1993</option>
												<option value="1992">1992</option>
												<option value="1991">1991</option>
												<option value="1990">1990</option>
												<option value="1989">1989</option>
												<option value="1988">1988</option>
												<option value="1987">1987</option>
												<option value="1986">1986</option>
												<option value="1985">1985</option>
												<option value="1984">1984</option>
												<option value="1983">1983</option>
												<option value="1982">1982</option>
												<option value="1981">1981</option>
												<option value="1980">1980</option>
												<option value="1979">1979</option>
												<option value="1978">1978</option>
												<option value="1977">1977</option>
												<option value="1976">1976</option>
												<option value="1975">1975</option>
												<option value="1974">1974</option>
												<option value="1973">1973</option>
												<option value="1972">1972</option>
												<option value="1971">1971</option>
												<option value="1970">1970</option>
												<option value="1969">1969</option>
												<option value="1968">1968</option>
												<option value="1967">1967</option>
												<option value="1966">1966</option>
												<option value="1965">1965</option>
												<option value="1964">1964</option>
												<option value="1963">1963</option>
												<option value="1962">1962</option>
												<option value="1961">1961</option>
												<option value="1960">1960</option>
												<option value="1959">1959</option>
												<option value="1958">1958</option>
												<option value="1957">1957</option>
												<option value="1956">1956</option>
												<option value="1955">1955</option>
												<option value="1954">1954</option>
												<option value="1953">1953</option>
												<option value="1952">1952</option>
												<option value="1951">1951</option>
												<option value="1950">1950</option>
												<option value="1949">1949</option>
												<option value="1948">1948</option>
												<option value="1947">1947</option>
												<option value="1946">1946</option>
												<option value="1945">1945</option>
												<option value="1944">1944</option>
												<option value="1943">1943</option>
												<option value="1942">1942</option>
												<option value="1941">1941</option>
												<option value="1940">1940</option>
												<option value="1939">1939</option>
												<option value="1938">1938</option>
												<option value="1937">1937</option>
												<option value="1936">1936</option>
												<option value="1935">1935</option>
												<option value="1934">1934</option>
												<option value="1933">1933</option>
												<option value="1932">1932</option>
												<option value="1931">1931</option>
												<option value="1930">1930</option>
												<option value="1929">1929</option>
												<option value="1928">1928</option>
												<option value="1927">1927</option>
												<option value="1926">1926</option>
												<option value="1925">1925</option>
												<option value="1924">1924</option>
												<option value="1923">1923</option>
												<option value="1922">1922</option>
												<option value="1921">1921</option>
												<option value="1920">1920</option>
												<option value="1919">1919</option>
												<option value="1918">1918</option>
												<option value="1917">1917</option>
												<option value="1916">1916</option>
												<option value="1915">1915</option>
												<option value="1914">1914</option>
												<option value="1913">1913</option>
												<option value="1912">1912</option>
												<option value="1911">1911</option>
												<option value="1910">1910</option>
												<option value="1909">1909</option>
												<option value="1908">1908</option>
												<option value="1907">1907</option>
												<option value="1906">1906</option>
												<option value="1905">1905</option>
												<option value="1904">1904</option>
												<option value="1903">1903</option>
												<option value="1902">1902</option>
												<option value="1901">1901</option>
												<option value="1900">1900</option>		
											</select>
										</div>											
									</div>
								</div> -->
								<div class="form-group">
									<label>Email *</label>
										<input class="form-control" type="email" name="email" value="<?php echo $email;?>">	
								</div>
								<div class="form-group">
									<label>Password *</label>
										<input class="form-control" type="password" name="password" value="<?php echo $password;?>">		
								</div>
								<div class="form-group">
									<label>Confirm Password *</label>
										<input class="form-control" type="password" name="confirm_password" value="<?php echo $confirm_password;?>">		
								</div>
																							
								<div class=" my-2">Gender *</div>
								<div class="form-check">
									<label class="form-check-label my-1">
										<input class="form-check-input" type="radio" name="gender" id="" value="<?php echo $gender;?>"><span class="pr-2">Male</span>
									</label>
									
									<label class="form-check-label my-1">
										<input class="form-check-input" type="radio" name="gender" id="" value="<?php echo $gender;?>"><span class="pr-2">Female</span>
									</label>	
								</div>

								<div class="dropdown-divider mb-3"></div> <!-- divider -->

								<div class="text-center">
									<button type="submit" class="btn btn-dark mb-4 mr-2">Sign Up</button>
									<a class="btn btn-secondary mb-4" href="<?php echo base_url('home')?>">Cancel</a>
									
								</div>
							<?php
							echo form_close();
							?>
						</div>
						<div class="col-md-3 col-sm-2 ">
							
						</div>
					</div>
				</div>
			</div>
		</div>