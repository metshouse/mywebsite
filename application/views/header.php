	
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
				<a class="navbar-brand" href="#">LOGO</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				
				<div class="collapse navbar-collapse" id="navbarNav" >			 		
			 		<form class="form-inline my-2 my-lg-0" >
					        <input class="form-control form-control-sm mr-sm-2" type="text" placeholder="Search" aria-label="Search" style="width:250px;">
					        <button class="btn btn-sm btn-outline-light my-2 my-sm-0" type="submit">Search</button>
					</form>
					<div class="pl-5">
						<ul class="navbar-nav mx-auto"> <!-- mx-auto = align center -->
							<li class="nav-item active pr-5">
								<a class="nav-link text-light" href="<?php echo base_url('home')?>" >Home <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item pr-5">
								<a class="nav-link text-light" href="#">Portfolio</a>
							</li>
							<li class="nav-item pr-5" >
								<a class="nav-link text-light" href="<?php echo base_url('store')?>">Store</a>
							</li>
						</ul>
					</div>
				</div>

				<button type="button" class="btn btn-outline-light btn-sm sign_in_button ml-5" data-toggle="modal" data-target="#myModal">Sign In</button> <!-- button Trigger modal-->
			</nav>


			<?php $this->load->view('login_page');?>