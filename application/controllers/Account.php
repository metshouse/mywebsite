<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct() {        
        parent::__construct();
    }


	public function index(){
		
	}

	public function forgot_account(){

		$this->load->view('head');
		$this->load->view('header');
		$this->load->view('forgotpage');
		$this->load->view('footer');
	}


	// User Registration

	public function register_account_page(){

		$this->load->view('head');
		$this->load->view('header');
		$this->load->view('registerpage');
		$this->load->view('footer');
	}

	

	public function validation_page(){

		$data['full_name'] = $this->input->post('full_name',TRUE);
		$data['birthday'] = $this->input->post('birthday',TRUE);
		$data['email'] = $this->input->post('email',TRUE);
		$data['password'] = $this->input->post('password',TRUE);
		$data['confirm_password'] = $this->input->post('confirm_password',TRUE);
		$data['gender'] = $this->input->post('gender',TRUE);
		$this->load->view('head');
		$this->load->view('header');
		$this->load->view('register_validation_page', $data);
		$this->load->view('footer');
	}
	public function sign_up_validation(){

		$this->form_validation->set_rules('full_name', 'Full Name', 'required');
		$this->form_validation->set_rules('birthday', 'Birthday', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		// $this->form_validation->set_rules('gender', 'Gender', 'required');
		if ($this->form_validation->run() == FALSE)
                {
                        $this->validation_page();
                }
                else
                {
                        $this->register_account();
                }
	}

	public function register_account(){

		$full_name = $this->input->post('full_name',TRUE);
		$birthday = $this->input->post('birthday',TRUE);
		$email = $this->input->post('email',TRUE);
		$password = $this->input->post('password',TRUE);
		$gender = $this->input->post('gender',TRUE);
		
		$md5password = md5($password);
		$this->load->model('Account_mdl');
		$this->Account_mdl->user_registration($full_name, $birthday, $email, $md5password, $gender);
		redirect('account/form_success');
	}

	public function form_success(){

		$this->load->view('head');
		$this->load->view('header');
		$this->load->view('form_success_page');
		$this->load->view('footer');
	}

	// END of user registration

	// User Sign In


	public function sign_in_validation(){

		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) 
		{
			redirect('home');
		}
		else
		{
			$this->conf_sign_in();
		}
	}	

	public function conf_sign_in(){

		$email = $this->input->post('email',TRUE);
		$password = $this->input->post('password',TRUE);
		$md5password = md5($password);
		$result = $this->to_get_account($email, $md5password);
		$account_row = $result->row();

		if ($result->num_rows() == "1") 
		{
		 	$newdata = array(
		 		'email' => $account_row->email,
		 		'user_id' => $account_row->user_id,
		 		'full_name' => $account_row->full_name,
		 		'logged_in' => TRUE
		 	);
		 	
		 	$this->session->set_userdata($newdata);
			

			redirect('home');
			
				
		}
		else 
		{
			redirect('queue/store');
				
		}


	}

	public function to_get_account(){

		$this->load->model('Account_mdl');
		$result = $this->Account_mdl->get_account($email, $md5password);
		return $result;
	}

	public function user_logout(){

		$this->session->unset_userdata('email');
		$this->session->unset_userdata('full_name');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('logged_in');
	}

	public function check_if_logged_in(){
		
		if (!$this->session->has_userdate('logged_in'))

		{
			redirect('home');
		}
	}

	// END of User Sign In


}