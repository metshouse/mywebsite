<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {


	public function index()

	{
		$this->load->view('head');
		$this->load->view('header');
		$this->load->view('storepage');
		$this->load->view('footer');
	}
}
